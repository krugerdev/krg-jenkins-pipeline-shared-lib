def call(body) {
	def pipelineParams = [:]
	body.resolveStrategy = Closure.DELEGATE_FIRST
	body.delegate = pipelineParams
	body()

	properties([
        durabilityHint('MAX_SURVIVABILITY'),
	])
	
	pipeline {
		agent {
			kubernetes {
				defaultContainer 'gradle'
        		label 'jdk8-slave'
        		yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: jdk8-jnlp-slave
      jenkins/jdk8-slave: true
  spec:
    containers:
    - name: gradle
      image: gradle:4.10.0-jdk8
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080 
      tty: true
  """
			}
		}
		
		environment {
		  NEXUS_COMMON_CREDS = credentials('nexuskruger')
		  JAVA_HOME = tool 'jdk8'
		  scannerHome = tool 'SonarScanner 4.3.0';
		}

		triggers {
			bitbucketPush() 
		}
		
		options {
			buildDiscarder(logRotator(numToKeepStr:'3'))
			timeout(time: 30, unit: 'MINUTES')
		}
	  
		stages {
			stage('Set up enviroment') {
				steps {
					script {
						env.GIT_REPO_NAME = "${env.GIT_URL}".tokenize('/').last().split("\\.")[0]
						env.GIT_USER_NAME = sh(script: "git show -s --pretty='%an' ${GIT_COMMIT}", returnStdout: true).trim()
						env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae' ${GIT_COMMIT}", returnStdout: true).trim()
						if ( env.BRANCH_NAME == 'master' ) {
							env.BUILD_ENVIROMENT = "-Penv=pro"
						}else {
							env.BUILD_ENVIROMENT = "-Penv=dev"
						}				
					}
				}
			}

			stage('Checkout') {
				steps {
					timestamps {
						checkout scm
					}
				}
			}

			stage('Pre Build') {
				steps {
					echo "Rama: ${env.GIT_BRANCH} / ${env.gitlabSourceBranch},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
					echo "Iniciando limpieza"
					sh 'gradle --console=plain clean -Dmaven.repo.login=$NEXUS_COMMON_CREDS_USR -Dmaven.repo.password=$NEXUS_COMMON_CREDS_PSW -x check -x test'
				}
			}
			
			stage('Build') {
				steps {
					echo "Iniciando construccion"
					script {
						echo "Iniciando construccion"
						sh "gradle --console=plain build --refresh-dependencies -Dmaven.repo.login=$NEXUS_COMMON_CREDS_USR -Dmaven.repo.password=$NEXUS_COMMON_CREDS_PSW -x test -x check ${env.BUILD_ENVIROMENT}"					
					}
				}
			}
			
			stage('Test') {		
				parallel {				
					stage('Unit Test') {					
						steps {
							script {
								try {
									echo "Iniciando test unitarios de la rama ${env.BRANCH_NAME}"
									sh 'gradle test --no-daemon'						
								} catch(Exception e) {
									echo "Error en los test unitarios de la rama ${env.BRANCH_NAME}"
								} finally {
									junit '**/build/test-results/test/*.xml' //Resultados de los test ejecutados
								}
							}
						}
					}
				}
			}

			stage('Code Quality - Sonarqube') {
            	steps {
					container('sonar-scanner') { 
                		echo 'Code quality'
                		withSonarQubeEnv('SonarQubeKruger') {
                 	 		sh "sonar-scanner"
                		}
              		}
				}
			}

			stage('Publish Artifact to Nexus') {
				steps {
					echo "Publish Artifact to Nexus"
					sh "gradle upload --no-daemon -Dmaven.repo.login=$NEXUS_COMMON_CREDS_USR -Dmaven.repo.password=$NEXUS_COMMON_CREDS_PSW -x test ${env.BUILD_ENVIROMENT}"
				}
			}
		}	
		
		post {
			always {
				echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
			}
			success {
				echo 'La linea de construccion finalizo exitosamente'
				slackSend(
					channel: "devopsci",
					color: "good",
					message: ":simple_smile::computer: *ÉXITO EN CONSTRUCCIÓN* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
				)
			}        
			failure {
				echo "La linea de construccion finalizo con errores ${currentBuild.fullDisplayName} with ${env.BUILD_URL}"
				slackSend (
					channel: "devopsci", 
					color: "danger", 
					message: ":disappointed_relieved::sos: *ERROR EN CONSTRUCCIÓN* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
				)
			}
			unstable {
				echo 'La linea de construccion finalizo de forma inestable' 
				slackSend (
					channel: "devopsci", 
					color: "warning", 
					message: ":worried::warning: *ALERTA CONSTRUCCIÓN INESTABLE* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
				)
			}
		}
	}
}	
	