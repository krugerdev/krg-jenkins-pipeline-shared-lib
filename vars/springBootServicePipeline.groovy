def notifyUnstable(slackNotificationChannel){
  echo 'La linea de construccion finalizo de forma inestable' 
  slackSend (
    channel: slackNotificationChannel, 
    color: "warning", 
    message: ":worried::warning: *ALERTA CONSTRUCCIÓN INESTABLE* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
  )
}
def notifySuccess(slackNotificationChannel){
  echo 'La linea de construccion finalizo exitosamente'
  slackSend(
    channel: slackNotificationChannel,
    color: "good",
    message: ":simple_smile::computer: *ÉXITO EN CONSTRUCCIÓN* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
  )
}
def notifyFailure(slackNotificationChannel){
  echo "La linea de construccion finalizo con errores ${currentBuild.fullDisplayName} with ${env.BUILD_URL}"
  slackSend (
    channel: slackNotificationChannel, 
    color: "danger", 
    message: ":disappointed_relieved::sos: *ERROR EN CONSTRUCCIÓN* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
  )
}

def call(body) {
  def pipelineParams = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()

  pipeline {
    agent {
      kubernetes {
        label 'angular-slave'
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: angular8-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: angular8-build-env
      image: docker.krugernetes.com/ec.com.kruger.jenkinsci/angular8-jnlp-slave:1.2.0
      imagePullPolicy: IfNotPresent
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
      resources:
        requests:
          memory: "1000Mi"
          cpu: "1000m"
        limits:
          memory: "2000Mi"
          cpu: "2000m"
    - name: docker
      image: docker:latest
      command:
      - cat
      tty: true
      resources:
        requests:
          memory: "200Mi"
          cpu: "200m"
        limits:
          memory: "400Mi"
          cpu: "500m"
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: regdock  
  """
      }
    }

    environment {
      registryCredential = 'nexuskruger'
      JAVA_HOME = tool 'jdk8'
      scannerHome = tool 'SonarScanner 4.2.0';
    }

    triggers {
      bitbucketPush() 
    }
    
    options {
      disableConcurrentBuilds()
      skipStagesAfterUnstable()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 15, unit: 'MINUTES')
    }

    stages {
      stage('Set up environment') {
        steps {
          container('angular8-build-env') {
            script {
              echo "ENV: ${env} ${pipelineParams.env} ${pipelineParams.slackNotificationChannel}"
              env.GIT_REPO_NAME = "${env.GIT_URL}".tokenize('/').last().split("\\.")[0]
              if (env.BRANCH_NAME != 'master' && env.CHANGE_ID == null){
                env.GIT_USER_NAME = sh(script: "git show -s --pretty='%an' ${env.GIT_COMMIT}", returnStdout: true).trim()
                env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae' ${env.GIT_COMMIT}", returnStdout: true).trim()
              }
              env.VERSION_APP = sh(script: "npm run-script version |tail -1", returnStdout: true).trim()
              env.NAME_APP = sh(script: "npm run-script name |tail -1", returnStdout: true).trim()
              env.DOCKER_REGISTRY = sh(script: "npm run-script dockerRegistry |tail -1", returnStdout: true).trim()
              env.DOCKER_REGISTRY_IMAGE = env.DOCKER_REGISTRY +'/'+ env.NAME_APP.replace('@','') +':'+ env.VERSION_APP
              env.DOCKER_REGISTRY_IMAGE_LATEST = env.DOCKER_REGISTRY +'/'+ env.NAME_APP.replace('@','') +':latest'
              echo "Usuario: ${env.BUILD_USER}"
              echo "Email: ${env.GIT_USER_EMAIL}"
            }
          }
        }
      }

      stage('Checkout code') {      
        steps {        
          echo "Cambios: ${env.GIT_REPO_NAME}"
          echo 'Checkout code'
          checkout scm
          sh 'printenv'
        }
      }

        stage('Install dependencies') {
          steps {
            container('angular8-build-env') {
              echo 'Install NPM dependencies'
              sh '''
                  npm install -f --no-package-lock --verbose -d --quiet
              '''
            }
          }
        }
      
      //stage('Test/QC') {		
        //parallel {
          stage('Unit Test') {
            steps {
              container('angular8-build-env') {
              echo 'Test project'
              sh '''
                npm run-script test:ci
              '''
              }
            }
            post {
              always {
                junit allowEmptyResults: true, testResults: '**/test-results.xml'               
              }
            }
          }

          stage('Code Quality - Lint') {
            steps {
              container('angular8-build-env') {         
                echo 'Code quality'
                sh 'npm run-script lint:ci'
              }
            }
            post {
              always {
                  recordIssues enabledForFailure: true, aggregatingResults: true, tool: checkStyle(pattern: 'checkstyle-result.xml')
              }
            }
          }

          stage('Code Quality - Sonarqube') {
            steps {
              container('angular8-build-env') { 
                echo 'Code quality'
                withSonarQubeEnv('SonarQubeKruger') {
                  sh "npm run-script test:code-coverage && ${scannerHome}/bin/sonar-scanner"
                }
              }
            }
          }

          stage('Code Quality - Security') {
            steps {
              container('angular8-build-env') { 
                script {
                  try {
                    sh 'npm run-script test:security'
                  }catch (err) {
                    unstable('Code Quality - Security failed!')
                  }
                }
              }
            }
          }

        //}
      //}

      stage('Build') {
        steps {
          container('angular8-build-env') { 
            echo 'Start build project'
            sh 'npm run-script build:ci'
            sh '(ls -la dist/)'
          }
        }
      }

      stage('Build docker') {
        steps {
          container('docker') { 
            echo 'Start build docker'
            sh 'docker version'
            sh "docker build --network=host -t ${env.DOCKER_REGISTRY_IMAGE} --file ./DockerfileCI ."
            sh "docker tag ${env.DOCKER_REGISTRY_IMAGE} ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
          }
        }
      }    

      stage('Publish registry') {
        steps {
          container('docker') { 
            script {
              docker.withRegistry('https://'+env.DOCKER_REGISTRY, registryCredential ) {
                echo 'Start publish registry'
                sh "docker push ${env.DOCKER_REGISTRY_IMAGE}"
                sh "docker push ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
              }
            }
          }
        }
      }
    }	
    
    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        notifySuccess(pipelineParams.slackNotificationChannel)
      }        
      failure {
        notifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        notifyUnstable(pipelineParams.slackNotificationChannel)
      }
    }
  }  
}