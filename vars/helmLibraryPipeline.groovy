def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()

  def labelgenerated = "helm3-sb-slave-${UUID.randomUUID().toString()}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED') 
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: helm3-slave
      jenkins/helm3-slave: true
  spec:
    containers:
    - name: git
      image: alpine/git:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: krg-regcred
  """
      }
    }

    environment {
      registryCredential = 'krg-nexus-jenkins'
      NEXUS_CREDENTIAL = credentials('krg-nexus-jenkins')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 20, unit: 'MINUTES')
    }

    stages {
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          container('git') {
            script {
              abortAllPreviousBuildInProgress(currentBuild)

              env.GIT_URL = sh(returnStdout: true, script: "git config --get remote.origin.url").trim()
              env.GIT_REPO_NAME = "${env.GIT_URL}".tokenize('/').last().split("\\.")[0]
              env.GIT_SHORT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
              env.GIT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%H'").trim()
              env.GIT_COMMIT_MSG = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%s'").trim()

              if (env.CHANGE_ID == null) {
                env.GIT_USER_NAME = sh(script: "git show -s --pretty='%an' ${env.GIT_COMMIT}", returnStdout: true).trim()
                env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae' ${env.GIT_COMMIT}", returnStdout: true).trim()
              }

              env.BRANCH_PREFIX = "${env.BRANCH_NAME}".tokenize('/').first()

              env.ROOT_DIRECTORY = pipelineParams.rootDirectory
              if (env.ROOT_DIRECTORY == null || env.ROOT_DIRECTORY == 'null') {
                env.ROOT_DIRECTORY = "."
              }
              sh 'printenv'
            }
          }

        }
      }

      stage('Code Quality - Helm Lint') {
        steps {
          container('helm-kubectl') {
            echo 'Start lint helm chart'
            script {
              try {
                sh "helm repo add krg-helm https://nexus.krugernetes.com/repository/helm/ --username ${env.NEXUS_CREDENTIAL_USR} --password ${env.NEXUS_CREDENTIAL_PSW}"
                retry(3) {
                  sh "helm dep update ."
                }
                sh "helm lint --strict ."
              } catch(err) {
                unstable('Code Quality - Helm Lint failed!')
              }
            }
          }
        }
      }

      stage('Package helm chart') {
        steps {
            container('helm-kubectl') {
              script {
                echo 'Start build helm chart'
                sh "helm repo add krg-helm https://nexus.krugernetes.com/repository/helm/ --username ${env.NEXUS_CREDENTIAL_USR} --password ${env.NEXUS_CREDENTIAL_PSW}"
                retry(3) {
                  sh 'helm dep update .'
                }
                sh 'helm package .'
              }
            }
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' || env.BRANCH_PREFIX == 'release' || env.BRANCH_PREFIX == 'hotfix'
          }
        }
        steps {
          container('helm-kubectl') {
            script {

              echo 'Start publish helm chart'
              sh 'helm plugin install --version master https://github.com/sonatype-nexus-community/helm-nexus-push.git'
              sh "helm repo add krg-helm https://nexus.krugernetes.com/repository/helm/ --username ${env.NEXUS_CREDENTIAL_USR} --password ${env.NEXUS_CREDENTIAL_PSW}"
              retry(3) {          
                sh "helm nexus-push krg-helm .  -u ${env.NEXUS_CREDENTIAL_USR} -p ${env.NEXUS_CREDENTIAL_PSW}"
              }

            }
          }
        }
      }

    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}
