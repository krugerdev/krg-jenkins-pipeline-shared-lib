def String[] call(level=1, n=1) {
  def directories = sh(returnStdout: true, script: "git log -n $n --name-status | awk '/(A|M|D)/ {print \$2;}' | grep -E -i -o '^(\\w+/){$level}' | sort | uniq").trim().split( '\n')
  echo "directories changed ${directories}"
  return directories
}

