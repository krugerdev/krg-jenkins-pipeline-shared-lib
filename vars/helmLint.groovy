def call(Map config = [: ]) {
  container('helm-kubectl') {
    echo 'Start lint helm chart'

    sh "helm repo add kruger-helm https://nexus.krugernetes.com/repository/helm/ --username ${env.DOCKER_CREDENTIAL_USR} --password ${env.DOCKER_CREDENTIAL_PSW}"

    script {
      if (env.HAS_SUBPROJECTS == "false") {
        startLintHelmChart("./ci/helm")
      } else {
        def projectToPub = env.PUBLISH_APP_PATHS.split('\n')
        for (projectPath in projectToPub) {
          echo "Lint helm chart sub ${projectPath}"
          startLintHelmChart("./${projectPath}/ci/helm");
        }
      }
    }
  }
}

def startLintHelmChart(String helmFolder) {
  try {
    retry(3) {
      sh "helm dep update ${helmFolder}"
    }
    sh "helm lint --strict ${helmFolder}"
  } catch(err) {
    unstable('Code Quality - Helm Lint failed!')
  }
}